### What is it?

Setup to use runit-init with OpenRC.

runit-init is used for booting, which transfers control to OpenRC for
bringing up the filesystem, loading modules, running udev, etc.
Then control is passed back to runit for starting and maintaining the system services.

Check [hints](hints.txt) for what to do after installation.

### Links

* Source: https://github.com/gentoo/gentoo/tree/master/sys-process/runit/files
* Documentation: https://wiki.gentoo.org/wiki/Runit#Runit_as_the_init_system
* Services: https://github.com/aadityabagga/runit-services/tree/update
