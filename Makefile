# adapted from
# https://github.com/void-linux/void-runit/blob/master/Makefile
# https://github.com/gentoo/gentoo/blob/master/sys-process/runit/runit-2.1.2.ebuild

PREFIX ?=	/usr/local
SCRIPTS=	1 2 3
SOURCE_GEN=	gentoo/sys-process/runit/files
SOURCE_VOID=	void-runit/services
NAME=		runit-init-openrc
DOCDIR= 	/usr/share/doc/$(NAME)
SERVICE_DIR=	/var/service

install:
	install -d ${DESTDIR}/etc/runit
	for script in ${SCRIPTS}; do \
		install -m755 files/etc/$$script ${DESTDIR}/etc/runit/$$script; \
		sed -i -e "s:/etc/service:${SERVICE_DIR}:g" ${DESTDIR}/etc/runit/$$script; \
	done
	install -m755 files/etc/ctrlaltdel ${DESTDIR}/etc/runit/ctrlaltdel
	install -d ${DESTDIR}/etc/sv
	for tty in tty1 tty2 tty3 tty4 tty5 tty6; do \
		install -d ${DESTDIR}/etc/sv/getty-$$tty; \
		install -m755 ${SOURCE_VOID}/agetty-generic/finish ${DESTDIR}/etc/sv/getty-$$tty/finish; \
		install -m755 files/etc/sv/agetty-generic/run ${DESTDIR}/etc/sv/getty-$$tty/run; \
		install -m644 ${SOURCE_VOID}/agetty-tty1/conf ${DESTDIR}/etc/sv/getty-$$tty/conf; \
	done
	install -d ${DESTDIR}/etc/sv/sulogin
	install -m755 ${SOURCE_VOID}/sulogin/run ${DESTDIR}/etc/sv/sulogin/run
	install -d ${DESTDIR}/etc/runit/runsvdir
	install -d ${DESTDIR}/etc/runit/runsvdir/single
	ln -s /etc/sv/sulogin ${DESTDIR}/etc/runit/runsvdir/single/
	install -d ${DESTDIR}/etc/runit/runsvdir/default
	for tty in tty1 tty2 tty3 tty4 tty5 tty6; do \
		ln -s /etc/sv/getty-$$tty ${DESTDIR}/etc/runit/runsvdir/default/; \
	done
	install -d ${DESTDIR}$(DOCDIR)
	install -m 644 README.md hints.txt COPYING ${DESTDIR}$(DOCDIR)

#uninstall:
#	rm ${DESTDIR}/etc/runit/runsvdir/current
