#!/bin/bash
# download.sh: downloads openrc scripts from gentoo sources

# for safety and robustness
set -e

# https://github.com/gentoo/gentoo/tree/master/sys-process/runit/files
REMOTE_REPO="gentoo"
REMOTE_REPO_URL="rsync://rsync.de.gentoo.org/gentoo-portage"

log="logs/download.log"
# Check for log directory
[ ! -d logs ] && mkdir logs
# Initialize log with date
date +"%F_%T" >> $log

# Checkout the source and write to log
mkdir -p "$REMOTE_REPO"
rsync -av --exclude '*.patch' --prune-empty-dirs --include-from "rsync-include-list" --exclude '*' "$REMOTE_REPO_URL/*" "$REMOTE_REPO/" 2>&1 | tee -a "$log"

# get files from other repositories
# https://github.com/void-linux/void-runit
REMOTE_REPO="void-runit"

mkdir -p "${REMOTE_REPO}/services/sulogin"
wget "https://raw.githubusercontent.com/void-linux/${REMOTE_REPO}/master/services/sulogin/run" -O "${REMOTE_REPO}/services/sulogin/run"

mkdir -p "${REMOTE_REPO}/services/agetty-generic"
wget "https://raw.githubusercontent.com/void-linux/${REMOTE_REPO}/master/services/agetty-generic/run" -O "${REMOTE_REPO}/services/agetty-generic/run"
wget "https://raw.githubusercontent.com/void-linux/${REMOTE_REPO}/master/services/agetty-generic/finish" -O "${REMOTE_REPO}/services/agetty-generic/finish"

mkdir -p "${REMOTE_REPO}/services/agetty-tty1"
wget "https://raw.githubusercontent.com/void-linux/${REMOTE_REPO}/master/services/agetty-tty1/conf" -O "${REMOTE_REPO}/services/agetty-tty1/conf"

wget "https://raw.githubusercontent.com/void-linux/${REMOTE_REPO}/master/1" -O "${REMOTE_REPO}/1"
wget "https://raw.githubusercontent.com/void-linux/${REMOTE_REPO}/master/2" -O "${REMOTE_REPO}/2"
wget "https://raw.githubusercontent.com/void-linux/${REMOTE_REPO}/master/3" -O "${REMOTE_REPO}/3"
wget "https://raw.githubusercontent.com/void-linux/${REMOTE_REPO}/master/rc.conf" -O "${REMOTE_REPO}/rc.conf"
wget "https://raw.githubusercontent.com/void-linux/${REMOTE_REPO}/master/ctrlaltdel" -O "${REMOTE_REPO}/ctrlaltdel"

# https://gitlab.com/flussence/overlay/tree/master/sys-process/runit/files
REMOTE_REPO="flussence"

mkdir -p "${REMOTE_REPO}/overlay/sys-process/runit/files"
wget "https://gitlab.com/${REMOTE_REPO}/overlay/raw/master/sys-process/runit/files/1" -O "${REMOTE_REPO}/overlay/sys-process/runit/files/1"

exit $?
